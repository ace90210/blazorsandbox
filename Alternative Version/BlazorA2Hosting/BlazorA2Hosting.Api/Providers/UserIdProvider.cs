﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Linq;

namespace BlazorA2Hosting.Api.Providers
{
    public class UserIdProvider : IUserIdProvider
    {
        public string GetUserId(HubConnectionContext connection)
        {
            var subjectClaim = connection.GetHttpContext().User.Claims.ToList().FirstOrDefault(c => c.Type.ToLower().Equals("sub"));

            return subjectClaim?.Value;
        }
    }
}
