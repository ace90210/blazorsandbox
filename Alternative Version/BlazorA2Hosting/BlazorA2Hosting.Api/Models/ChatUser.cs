﻿using System.Collections.Generic;

namespace BlazorA2Hosting.Api.Models
{
    public class ChatUser
    {
        public string MainGroup { get; set; }

        public string ConnectionId { get; set; }
         
        public string Username { get; set; }
    }
}
