﻿using System.Collections.Generic;

namespace BlazorA2Hosting.Api.Models
{
    public class ChatGroup
    {
        public bool IsPrivate { get; set; }

        public string Name { get; set; }
    }
}
