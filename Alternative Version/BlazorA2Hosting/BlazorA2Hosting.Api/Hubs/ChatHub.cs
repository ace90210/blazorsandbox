﻿using BlazorA2Hosting.Api.Models;
using BlazorA2Hosting.Shared;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorA2Hosting.Api.Hubs
{
    public class ChatHub : Hub
    {
        private static readonly string s_defaultGroup = "Lobby";

        private static ConcurrentDictionary<string, ChatUser> users = new ConcurrentDictionary<string, ChatUser>();

        private object userDictionaryLock = new object();

        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public async Task SendUpdate(IEnumerable<WeatherForecast> forecast)
        {
            await Clients.All.SendAsync("AsyncTaskCompleted", forecast);
        }

        public async Task JoinLobby(string user)
        {

            await Groups.AddToGroupAsync(Context.ConnectionId, s_defaultGroup);
            await Clients.All.SendAsync("ChatEvent", $"{user} Entered the {s_defaultGroup} room.");
            await base.OnConnectedAsync();
        }

        public override async Task OnConnectedAsync()
        {
            string username = Context.User.Identity.Name ?? "Anon";
            await Clients.All.SendAsync("ChatEvent", $"{Context.ConnectionId} Entered Chat as {username}");
            await base.OnConnectedAsync();
        }

        private async Task AddUserFromCurrentConnection(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentException("User must have a name");
            }
            string removedUsername = null;
            lock (userDictionaryLock)
            {
                //check username not taken
                foreach (var key in users.Keys)
                {
                    if (users[key].Username.ToLower().Equals(username.ToLower()))
                        throw new Exception("Username Taken!");
                }

                users.TryGetValue(Context.ConnectionId, out ChatUser user);

                if (user == null)
                {
                    var newUser = new ChatUser()
                    {
                        ConnectionId = Context.ConnectionId,
                        Username = username,
                        MainGroup = s_defaultGroup
                    };

                    users.TryAdd(Context.ConnectionId, newUser);

                }
                else
                {
                    removedUsername = user.Username;
                    user.Username = username;
                }
            }

            if (removedUsername != null)
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, removedUsername);

            await Groups.AddToGroupAsync(Context.ConnectionId, username);

            await Clients.All.SendAsync("ChatEvent", $"{Context.ConnectionId} Entered Chat as {username}");
        }

        private async Task RemoveCurrentUser(string username)
        {
            string removedUserName = null;
            lock (userDictionaryLock)
            {
                users.TryRemove(Context.ConnectionId, out ChatUser removedUser);
                removedUserName = removedUser?.Username;
            }

            if(removedUserName!=null)
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, removedUserName);
        }

        private List<ChatUser> GetUsersInGroup(string group)
        {
            List<ChatUser> groupUserList = new List<ChatUser>();

            foreach (var user in users.Values)
            {
                if(user.MainGroup?.Equals(group) ?? false)
                {
                    groupUserList.Add(user);
                }
            }
            return groupUserList;
        }

        private async Task MoveCurrentUserToGroupMain(string newGroup)
        {

            var chatUser = users[Context.ConnectionId];

            if (!string.IsNullOrWhiteSpace(chatUser.MainGroup))
            {
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, chatUser.MainGroup);
                await Clients.All.SendAsync("ChatEvent", $"{chatUser.Username} left group {chatUser.MainGroup}.");
            }
                
            chatUser.MainGroup = newGroup;
            await Groups.AddToGroupAsync(Context.ConnectionId, newGroup);
            await Clients.All.SendAsync("ChatEvent", $"{chatUser.Username} entered the {chatUser.MainGroup} room.");
        }
    }
}