﻿using System;

namespace BlazorA2Hosting.Shared
{
    public class MessageData
    {
        public string Username { get; set; }

        public string Message { get; set; }

        public TimeSpan TimeSinceLast { get; set; }
    }
}
