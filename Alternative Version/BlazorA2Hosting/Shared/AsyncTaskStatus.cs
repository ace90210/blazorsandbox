﻿namespace BlazorA2Hosting.Shared
{
    public enum AsyncTaskStatus
    {
        NotStarted,
        InProgress,
        Completed,
        Failed
    }
}
