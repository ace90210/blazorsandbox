﻿namespace BlazorA2Hosting.Shared
{
    public class AsyncTaskUpdate
    {
        public string TimeEstimate { get; set; }

        public string Status { get; set; }
    }
}
