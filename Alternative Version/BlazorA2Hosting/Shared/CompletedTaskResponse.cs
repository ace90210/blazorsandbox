﻿namespace BlazorA2Hosting.Shared
{
    public class CompletedTaskResponse<T>
    {
        public AsyncTaskStatus Status { get; set; }

        public string ErrorMessage { get; set; }

        public T Value { get; set; }
    }
}
