﻿using LocalStorage.JsonConverters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text.Json;

namespace LocalStorage
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddLocalStorage(this IServiceCollection services)
        {
            return services
                .AddScoped(context =>
                {
                    var options = new JsonSerializerOptions();

                    options.Converters.Add(new TimespanJsonConverter());
                    return options;
                })
                .AddScoped<ILocalStorageServiceAsync, LocalStorageServiceAsync>()
                .AddScoped<ILocalStorageServiceSync, LocalStorageServiceSync>();
        }


        public static IServiceCollection AddLocalStorageWithJsonSerializerOptions(this IServiceCollection services, JsonSerializerOptions options)
        {
            return services
                .AddScoped(context =>
                {
                    foreach(var option in options.Converters)
                    {
                        if (option.CanConvert(typeof(TimeSpan)))
                        {
                            return options;
                        }
                    }

                    options.Converters.Add(new TimespanJsonConverter());
                    return options;
                })
                .AddScoped<ILocalStorageServiceAsync, LocalStorageServiceAsync>()
                .AddScoped<ILocalStorageServiceSync, LocalStorageServiceSync>();
        }
    }
}
