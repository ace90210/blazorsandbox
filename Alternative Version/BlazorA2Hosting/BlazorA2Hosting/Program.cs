﻿using BlazorA2Hosting.HelperExtensions;
using BlazorA2Hosting.Models;
using BlazorA2Hosting.Services;
using Microsoft.AspNetCore.Blazor.Hosting;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlazorA2Hosting
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.Services.AddSingleton(async p =>
            {
                var httpClient = p.GetRequiredService<HttpClient>();
                var result = await httpClient.GetJsonAsync<AppSettings>("appsettings.json").ConfigureAwait(false);
                return result;
            });

            builder.Services.AddScoped(context =>
            {
                return new SignalRFactory("https://localhost:44380");
            });

            builder.UseStartup<Startup>();

            builder.RootComponents.Add<App>("app");

            await builder.Build().RunAsync();
        }
    }
}
