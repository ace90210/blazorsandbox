﻿namespace BlazorA2Hosting.Models
{
    public class AppSettings
    {
        /// <summary>
        /// Api Url
        /// https://blazorapi1.thecodehoster.com/
        /// </summary>
        public string ApiUrl { get; set; }
    }
}
