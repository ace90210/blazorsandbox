﻿using Microsoft.Extensions.DependencyInjection;

namespace BlazorA2Hosting.HelperExtensions
{
    public interface IStartup
    {
        void ConfigureServices(IServiceCollection services);
    }
}
