﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace BlazorA2Hosting.HelperExtensions
{
    public static class HttpExtensions
    {
        public static async Task<HttpResponseMessage> PostJsonTrackedAsync(this HttpClient client, string uri, object content)
        {
            var stringContent = new StringContent(JsonSerializer.Serialize(content), System.Text.Encoding.UTF8, "application/json");

            return await client.PostAsync(uri, stringContent);
        }

        public static async Task<T> GetContentInstanceAsync<T>(this HttpResponseMessage httpResponseMessage)
        {
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                try
                {
                    var responseContentString = await httpResponseMessage.Content.ReadAsStreamAsync();

                    return await JsonSerializer.DeserializeAsync<T>(responseContentString, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                }
                catch (Exception)
                {
                    return default(T);
                }
            }

            return default(T);
        }
    }    
}
