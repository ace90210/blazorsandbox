﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorA2Hosting.Services
{
    public class SignalRFactory
    {
        private ConcurrentDictionary<string, HubConnection> hubConnectionRegister = new ConcurrentDictionary<string, HubConnection>();
        private string _apiPath;

        public SignalRFactory(string apiPath)
        {
            _apiPath = apiPath;
        }

        public async Task<HubConnection> GetOrCreateHubConnection(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Must Provide a SignalR path");
            }
            Console.WriteLine("Hub Add/Create: " + path);

            return hubConnectionRegister.GetOrAdd(path, c => 
            {
                Console.WriteLine($"Hub \"{path}\" Not Found Creating...");

                var hub = new HubConnectionBuilder()
                    .WithUrl($"{_apiPath}{path}")
                    .Build();

                Console.WriteLine($"Hub \"{path}\" Created. Starting...");
                hub.StartAsync().GetAwaiter();
                Console.WriteLine($"Hub \"{path}\" Started");
                return hub;
            });
        }
    }
}
