﻿using BlazorApp1.Server.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp1.Server.Services
{
    public class LoginService
    {
        public ConcurrentDictionary<string, ChatUser> Users { get; private set; } = new ConcurrentDictionary<string, ChatUser>();

        public LoginService()
        {

        }

        public bool UsernameExists(string username)
        {
            return Users.ContainsKey(username);
        }

        public string GetUsername(string connectionId)
        {
            var existingUser = Users.FirstOrDefault(u => u.Value.ConnectionId == connectionId).Value;

            if (existingUser != null)
                return existingUser.Username;

            return null;
        }

        public ChatUser Login(string connectionId, string username)
        {
            var newUser = new ChatUser()
            {
                Username = username,
                ConnectionId = connectionId
            };

            bool userAddedSuccessfully = Users.TryAdd(username, newUser);

            if (!userAddedSuccessfully)
            {
                throw new ArgumentOutOfRangeException("User Already Exists");
            }

            return newUser;
        }

        public ChatUser Logout(string connectionid)
        {
            var existingUser = Users.FirstOrDefault(u => u.Value.ConnectionId == connectionid).Key;

            if (existingUser != null)
            {
                Users.TryRemove(existingUser, out ChatUser user);
                return user;
            }

            return null;
        }

        public List<string> GetUsernames()
        {
            return Users.Keys?.ToList();
        }
    }
}
