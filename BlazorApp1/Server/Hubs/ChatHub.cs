﻿using BlazorApp1.Server.Services;
using BlazorApp1.Shared;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorApp1.Server.Hubs
{
    public class ChatHub : Hub
    {
        private readonly LoginService loginService;

        public ChatHub(LoginService loginService)
        {
            this.loginService = loginService ?? throw new ArgumentNullException(nameof(loginService));
        }

        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }
        public async Task SendEvent(string eventName, string user, string data)
        {
            await Clients.All.SendAsync("Event", eventName, user, data);
        }

        public async Task SendUpdate(IEnumerable<WeatherForecast> forecast)
        {
            await Clients.All.SendAsync("AsyncTaskCompleted", forecast);
        }

        public async Task Typing(string username, bool beginning)
        {
            if (username != null)
            {
                await Clients.All.SendAsync("Event", "Typing", username, beginning.ToString());
            }
        }

        public async Task Logout()
        {
            var loggedOutUser = loginService.Logout(Context.ConnectionId);
            if (loggedOutUser != null)
            {
                await SendEvent("Logout", loggedOutUser.Username, "Logged Out");
            }
        }
        public async Task LoginNotice(string username, string data)
        {
            {
                await SendEvent("Login", username, data);
            }
        }

        public List<string> Login(string connectionId, string username)
        {
            try
            {
                loginService.Login(connectionId, username);

                return loginService.GetUsernames();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> GetUserList()
        {
            return loginService.GetUsernames();
        }

        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var loggedOutUser = loginService.Logout(Context.ConnectionId);

            if(loggedOutUser != null)
            {
                await SendEvent("Logout", loggedOutUser.Username, "Disconnected");
            }

            await base.OnDisconnectedAsync(exception);
        }
    }
}