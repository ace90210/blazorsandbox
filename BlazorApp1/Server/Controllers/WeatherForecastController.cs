﻿using BlazorApp1.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading;
using BlazorApp1.Server.Hubs;
using Microsoft.AspNetCore.SignalR;
using BlazorApp1.Server.Models;
using System.Collections.Concurrent;

namespace BlazorApp1.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> logger;
        private readonly IHubContext<ChatHub> hubcontext;

        private static ConcurrentBag<AsyncTask> Tasks = new ConcurrentBag<AsyncTask>();

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IHubContext<ChatHub> hubcontext)
        {
            this.logger = logger;
            this.hubcontext = hubcontext;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }



        [HttpGet]
        [Route("status/{id}")]
        public ActionResult<AsyncTaskUpdate> GetStatus(Guid id)
        {
            var task = Tasks.FirstOrDefault(t => t.Id == id);

            if(task != null)
            {
                return Ok(new AsyncTaskUpdate()
                {
                    Status = Enum.GetName(typeof(AsyncTaskStatus), task.Status),
                    TimeEstimate = task.TimeEstimate?.ToString()                    
                });
            }

            return NotFound();
        }


        [HttpPost]
        public ActionResult<AsyncResponse> PostWithAsyncResponse(List<string> summaries)
        {
            return PostWithAsyncResponse(null, summaries);
        }

        [HttpPost]
        [Route("{connectionid}")]
        public ActionResult<AsyncResponse> PostWithAsyncResponse(string connectionid, List<string> summaries)
        {
            if (summaries?.Count <= 2)
            {
                return BadRequest("Not Enough Items");
            }

            Random rnd = new Random();

            TimeSpan waitTime = new TimeSpan(0, 0, rnd.Next(15, 59));
            
            var delayedResponse = new AsyncResponse()
            {
                Id = Guid.NewGuid().ToString(),
                TimeEstimate = (waitTime + waitTime * (rnd.Next(-10, 10) / 100.0f)).ToString(),
                Status = Enum.GetName(typeof(AsyncTaskStatus), AsyncTaskStatus.InProgress),
                StatusEndpoint = "/weatherforecast/status"
            };

            AsyncTask newTask = new AsyncTask()
            {
                Id = new Guid(delayedResponse.Id),
                Created = DateTime.Now,
                Status = AsyncTaskStatus.InProgress,
                Timeout = new TimeSpan(0, 1, 0),
                TimeEstimate = TimeSpan.Parse(delayedResponse.TimeEstimate)
            };
            Tasks.Add(newTask);

            Task.Run(async () =>
            {
                while (DateTime.Now < newTask.Created.Add(waitTime) && newTask.Status != AsyncTaskStatus.Failed)
                {
                    Thread.Sleep(500);
                    newTask.Progress();
                }

                if(newTask.Status == AsyncTaskStatus.InProgress)
                {
                    Summaries = summaries.ToArray();
                    newTask.Status = AsyncTaskStatus.Completed;


                    if (!string.IsNullOrWhiteSpace(connectionid))
                    {
                        await hubcontext.Clients.Client(connectionid).SendAsync("AsyncTaskCompleted", new CompletedTaskResponse<IEnumerable<WeatherForecast>>()
                        {
                            Status = newTask.Status,
                            Value = Get()
                        });
                    }
                    else
                    {
                        await hubcontext.Clients.All.SendAsync("AsyncTaskCompleted", new CompletedTaskResponse<IEnumerable<WeatherForecast>>()
                        {
                            Status = newTask.Status,
                            Value = Get()
                        });
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(connectionid))
                    {
                        await hubcontext.Clients.Client(connectionid).SendAsync("AsyncTaskCompleted", new CompletedTaskResponse<IEnumerable<WeatherForecast>>()
                        {
                            Status = newTask.Status,
                            ErrorMessage = "Update Failed Due to Timeout"
                        });
                    }
                    else
                    {
                        await hubcontext.Clients.All.SendAsync("AsyncTaskCompleted", new CompletedTaskResponse<IEnumerable<WeatherForecast>>()
                        {
                            Status = newTask.Status,
                            ErrorMessage = "Update Failed Due to Timeout"
                        });
                    }
                }


                Console.WriteLine("Completed!");
            });

            return delayedResponse;
        }
    }
}
