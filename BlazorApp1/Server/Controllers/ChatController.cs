﻿using BlazorApp1.Server.Hubs;
using BlazorApp1.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp1.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ChatController : ControllerBase
    {
        private readonly ChatHub chatHub;

        public ChatController(ChatHub chatHub)
        {
            this.chatHub = chatHub;
        }

        public IActionResult Index()
        {
            return NoContent();
        }

        [HttpGet]
        [Route("login/{connectionId}/{username}")]
        public ActionResult<LoginResponse> Login(string connectionId, string username)
        {
            if (string.IsNullOrWhiteSpace(connectionId))
            {
                return BadRequest(new LoginResponse()
                {
                    ErrorMessage = "No Connection Id Provided."
                });
            }

            if (string.IsNullOrWhiteSpace(username))
            {
                return BadRequest(new LoginResponse()
                {
                    ErrorMessage = "Please Enter a Username."
                });
            }

            if (username.Length < 3 || username.Length > 10)
            {
                return BadRequest(new LoginResponse()
                {
                    ErrorMessage = "Username must be between 3 and 10 charecters long with no special characters!"
                });
            }

            if (username.Any(ch => !Char.IsLetterOrDigit(ch)))
            {
                return BadRequest(new LoginResponse()
                {
                    ErrorMessage = "Username must contain no special characters!"
                });
            }

            try
            {
                var users = chatHub.Login(connectionId, username);
                return Ok(new LoginResponse() { ActiveUsers = users });
            }
            catch (ArgumentOutOfRangeException)
            {
                return BadRequest(new LoginResponse()
                {
                    ActiveUsers = chatHub.GetUserList(),
                    ErrorMessage = "Username Taken, try another!"
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new LoginResponse()
                {
                    ErrorMessage = "Unknown Error: " + ex.Message
                });
            }
        }

        [HttpGet]
        [Route("login/users")]
        public ActionResult<LoginResponse> GetUsers()
        {
            try
            {
                var users = chatHub.GetUserList();
                return Ok(new LoginResponse() { ActiveUsers = users });
            }
            catch (Exception ex)
            {
                return BadRequest(new LoginResponse()
                {
                    ErrorMessage = "Unknown Error: " + ex.Message
                });
            }
        }
    }
}