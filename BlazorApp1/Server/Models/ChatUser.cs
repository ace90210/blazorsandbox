﻿namespace BlazorApp1.Server.Models
{
    public class ChatUser
    {
        public string ConnectionId { get; set; }

        public string Username { get; set; }
    }
}
