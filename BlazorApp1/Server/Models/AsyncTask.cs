﻿using BlazorApp1.Shared;
using System;

namespace BlazorApp1.Server.Models
{
    public class AsyncTask
    {
        public Guid Id { get; set; }

        public DateTime Created { get; set; }

        public AsyncTaskStatus Status { get; set; }

        public TimeSpan Timeout { get; set; }
        public TimeSpan? TimeEstimate { get; set; }

        public AsyncTaskStatus Progress()
        {
            if (Status == AsyncTaskStatus.InProgress)
            {
                if (DateTime.Now.Subtract(Created) >= Timeout)
                {
                    Status = AsyncTaskStatus.Failed;
                }
            }
            return Status;            
        }

        public void OnComplete(Action action)
        {
            action();
        }
    }
}
