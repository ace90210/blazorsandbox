﻿namespace BlazorApp1.Shared
{
    public class AsyncTaskUpdate
    {
        public string TimeEstimate { get; set; }

        public string Status { get; set; }
    }
}
