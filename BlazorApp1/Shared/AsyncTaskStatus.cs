﻿namespace BlazorApp1.Shared
{
    public enum AsyncTaskStatus
    {
        NotStarted,
        InProgress,
        Completed,
        Failed
    }
}
