﻿using System.Collections.Generic;

namespace BlazorApp1.Shared
{
    public class LogoutResponse
    {
        public string ErrorMessage { get; set; }
    }
}
