﻿using System;

namespace BlazorApp1.Shared
{
    public class MessageData
    {
        public Guid Id { get; } = new Guid();
        public string Username { get; set; }

        public string Message { get; set; }

        public TimeSpan TimeSinceLast { get; set; }
    }
}
