﻿namespace BlazorApp1.Shared
{
    public class AsyncResponse : AsyncTaskUpdate
    {
        public string Id { get; set; }

        public string StatusEndpoint { get; set; }
    }
}
