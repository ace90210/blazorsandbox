﻿using System.Collections.Generic;

namespace BlazorApp1.Shared
{
    public class LoginResponse
    {
        public string ErrorMessage { get; set; }

        public List<string> ActiveUsers { get; set; }
    }
}
