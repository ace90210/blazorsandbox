﻿using BlazorApp1.Client.HelperExtensions;
using Microsoft.AspNetCore.Blazor.Hosting;
using System.Threading.Tasks;

namespace BlazorApp1.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.UseStartup<Startup>();

            builder.RootComponents.Add<App>("app");            

            await builder.Build().RunAsync();
        }
    }
}
