﻿window.helpers = {
    showPrompt: function (text) {
        return prompt(text, 'Type your name here');
    },
    displayWelcome: function (elementid, welcomeMessage) {
        document.getElementById(elementid).innerText = welcomeMessage;
    },
    scrollIntoViewById: function (elementId) {
        document.getElementById(elementId).scrollIntoView();
    },
    focusElement: function (id) {
        const element = document.getElementById(id);

        if (element)
            element.focus();
        else
            console.log("Error cannot focus on element " + id + " it was not found");
    },
    playSound: function (id) {
        let sound = document.getElementById(id)
        if (sound) {
            sound.play();
        } else {
            console.log("Error playing sound " + id + " not found");
        }
    }
};