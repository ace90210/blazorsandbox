﻿using BlazorApp1.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.JSInterop;
using LocalStorage;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using BlazorApp1.Client.HelperExtensions;
using System.Timers;
using BlazorApp1.Client.Models;

namespace BlazorApp1.Client.Pages.ChatArea
{
    public class ChatAreaComponentBase : ComponentBase
    {
        protected string _username = "";
        protected string _messageInput;
        protected bool loggedIn = false;
        protected bool focusMessageOnNextRender = false;
        protected List<MessageData> _messages = new List<MessageData>();
        protected HubConnection _hubConnection;
        protected List<MessageData> _toastMessages = new List<MessageData>();
        protected List<ChatUser> usersList = new List<ChatUser>();

        protected List<string> usersTyping = new List<string>();

        protected Timer timer = new Timer(2000);

        protected string loginError;

        public bool IsConnected =>
            _hubConnection?.State == HubConnectionState.Connected;

        [Inject]
        public IJSRuntime JSRuntime { get; set; }

        [Inject]
        public ILocalStorageServiceAsync Localstorage { get; set; }

        [Inject]
        public HttpClient Http { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        protected async Task<bool> ScrollToEndOfChat()
        {
            return await JSRuntime.InvokeAsync<bool>("helpers.scrollIntoViewById", "chat-end");
        }

        protected async Task FocusElement(string elementId)
        {
            await JSRuntime.InvokeVoidAsync("helpers.focusElement", elementId);
        }

        protected async Task LoginEnter(KeyboardEventArgs eventArgs)
        {
            if (eventArgs.Key == "Enter")        // fire on enter
            {
                await Login();
            }
        }

        protected async Task SendEnter(KeyboardEventArgs eventArgs)
        {
            await StartTyping();            

            if (!eventArgs.ShiftKey && eventArgs.Key == "Enter")        // fire on enter
            {
                await Send(false);
            }
        }

        protected async Task StartTyping()
        {
            if (timer.Enabled)
            {
                timer.Stop();
            }

            await _hubConnection?.InvokeAsync("Typing", _username, true);

            timer.Start();
        }

        protected async Task Login()
        {
            _messages?.Clear();
            if (Localstorage != null)
                await Localstorage.ClearAsync();

            await CallLoginApi();
        }

        protected async Task<bool> CallLoginApi()
        {
            _username = _username?.Trim();

            if (!string.IsNullOrWhiteSpace(_username))
            {
                var response = await Http.GetAsync($"api/chat/login/{_hubConnection.ConnectionId}/{_username}");
                loginError = null;

                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.GetContentInstanceAsync<LoginResponse>();

                    if (responseContent?.ActiveUsers == null)
                    {
                        loginError = "Failed Login Bad Response From Api (no content)";
                    }
                    else if (responseContent.ActiveUsers.Count < 1)
                    {
                        loginError = "Failed Login Bad Response From Api (no users)";
                    }
                    else
                    {
                        usersList.Clear();
                        usersList.AddRange(responseContent.ActiveUsers.Select(au => new ChatUser() { Username = au }));

                        await _hubConnection?.InvokeAsync("LoginNotice", _username, "Standard Login");

                        await Localstorage.SetItemAsync("username", _username);
                        loggedIn = true;
                        focusMessageOnNextRender = true;
                    }

                }
                else
                {
                    var responseContent = await response.GetContentInstanceAsync<LoginResponse>();

                    loginError = responseContent.ErrorMessage;
                }
            }
            else
            {
                loginError = "Please Enter a Username.";
            }
            return loginError != null;
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!loggedIn)
            {
                await FocusElement("login-input");
            }
            else if (focusMessageOnNextRender)
            {
                await FocusElement("message-input");
                focusMessageOnNextRender = false;
            }

            await base.OnAfterRenderAsync(firstRender);
        }

        protected async Task CleanupToast(Guid messageId)
        {
            await Task.Delay(TimeSpan.FromSeconds(4));
            int index = _toastMessages.FindIndex(t => t.Id == messageId);
            _toastMessages.RemoveAt(index);
        }

        protected override async Task OnInitializedAsync()
        {
            timer.Elapsed += new ElapsedEventHandler(async (s, e) => {
                await StoppedTyping();
            });

            await LoadPersistedState();

            _hubConnection = new HubConnectionBuilder()
                .WithUrl(NavigationManager.ToAbsoluteUri("/chatHub"))
                .Build();

            AddRecieveMessageListener();

            AddChatEventListener();

            await _hubConnection.StartAsync();

            if (!string.IsNullOrWhiteSpace(_username))
            {
                await ReconnectUser();
            }
        }

        protected async Task StoppedTyping()
        {
            await _hubConnection?.InvokeAsync("Typing", _username, false);
            timer.Stop();
        }

        protected async Task LoadPersistedState()
        {
            if (Localstorage != null && await Localstorage.ContainKeyAsync("messagesHistory"))
            {
                var existingMessages = await Localstorage.GetItemAsync<List<MessageData>>("messagesHistory");

                if (existingMessages != null && existingMessages.Count > 0)
                {
                    _messages.Clear();
                    _messages.AddRange(existingMessages);
                }
                _username = await Localstorage.GetItemAsync<string>("username");
            }
        }

        protected void AddRecieveMessageListener()
        {
            _hubConnection.On<string, string>("ReceiveMessage", async (user, message) =>
            {
                var encodedMsg = $"{user}: {message}";

                var timeSinceLast = new TimeSpan(0, new Random().Next(0, 59), new Random().Next(2, 59));

                var messageData = new MessageData()
                {
                    Message = message,
                    Username = user,
                    TimeSinceLast = timeSinceLast
                };

                await AddNewMessageToChat(messageData, false);
            });
        }

        protected async Task ReconnectUser()
        {
            var response = await Http.GetAsync($"api/chat/login/users");
            loginError = null;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.GetContentInstanceAsync<LoginResponse>();

                if (responseContent?.ActiveUsers == null)
                {
                    loginError = "Failed Login Bad Response From Api (no content)";
                }
                else
                {
                    if (responseContent.ActiveUsers.Contains(_username))
                    {
                        //already logged in just render chat and update user list to latest
                        usersList.Clear();
                        usersList.AddRange(responseContent.ActiveUsers.Select(au => new ChatUser() { Username = au }));
                        loggedIn = true;
                        focusMessageOnNextRender = true;
                    }
                    else
                    {
                        //relogin required auto relogin
                        await CallLoginApi();
                    }
                }
            }
            else
            {
                loginError = "Failed to load users list. You have been Logged out. Please try to login again";
            }
        }

        protected void AddChatEventListener()
        {
            _hubConnection.On<string, string, string>("Event", async (eventName, user, data) =>
            {
                string message = "Unknown Event occured " + eventName;
                bool showToast = false;
                switch (eventName)
                {
                    case "Logout":
                        {
                            var u = usersList.FirstOrDefault(u => u.Username == user);

                            if (u != null)
                            {
                                usersList.Remove(u);
                            }
                            message = $"left the room ({data}).";
                            showToast = user != _username;
                            await JSRuntime.InvokeVoidAsync("helpers.playSound", "alert");
                        }
                        break;
                    case "Login":
                        {
                            var u = usersList.FirstOrDefault(u => u.Username == user);
                            if (u == null)
                            {
                                usersList.Add(new ChatUser() { Username = user });
                            }

                            message = $"joined the room.";
                            showToast = user != _username;

                            await JSRuntime.InvokeVoidAsync("helpers.playSound", "alert");
                        }
                        break;
                    case "Typing": 
                        {
                            if (user != _username)
                            {
                                var u = usersList.FirstOrDefault(u => u.Username == user);

                                if (u != null)
                                {
                                    u.IsTyping = bool.Parse(data);
                                }
                            }

                            StateHasChanged();
                            return;
                        }  
                }



                var encodedMsg = $"{user}: {message}";

                var messageData = new MessageData()
                {
                    Message = message,
                    Username = user
                };

                await AddNewMessageToChat(messageData, showToast);
            });
        }

        protected async Task AddNewMessageToChat(MessageData messageData, bool showToast)
        {
            _messages.Add(messageData);

            await JSRuntime.InvokeVoidAsync("helpers.playSound", "notification");

            await Localstorage.SetItemAsync("messagesHistory", _messages);
            StateHasChanged();

            if (showToast)
            {
                _toastMessages.Add(messageData);
                StateHasChanged();
                // Clean up notification after it has displayed for four seconds
                await CleanupToast(messageData.Id);
            }

            await ScrollToEndOfChat();

        }

        protected async Task SendClicked()
        {
            await Send(true);
        }

        protected async Task LogoutClicked()
        {
            loginError = null;
            await _hubConnection?.InvokeAsync("Logout");
            var u = usersList.FirstOrDefault(u => u.Username == _username);
            usersList.Remove(u);
            _username = null;
            await Localstorage.RemoveItemAsync("username");
            
            loggedIn = false;
        }

        protected async Task<bool> Send(bool refocusInput)
        {
            if (!string.IsNullOrWhiteSpace(_messageInput))
            {
                await _hubConnection?.SendAsync("SendMessage", _username, _messageInput);
                _messageInput = "";

                if (refocusInput)
                {
                    await FocusElement("message-input");
                }

                return true;
            }
            return false;
        }
    }
}
