﻿using BlazorApp1.Client.HelperExtensions;
using LocalStorage;
using Microsoft.Extensions.DependencyInjection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BlazorApp1.Client
{
    public class Startup : IStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            var options = new JsonSerializerOptions();

            options.Converters.Add(new JsonStringEnumConverter());
            services.AddLocalStorageWithJsonSerializerOptions(options);
        }
    }
}
