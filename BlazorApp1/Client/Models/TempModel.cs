﻿using System.ComponentModel.DataAnnotations;

namespace BlazorApp1.Client.Models
{
    public class TempModel
    {
        [Required]
        [StringLength(10, ErrorMessage = "Name is too long.")]
        public string Item1 { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "Name is too long.")]
        public string Item2 { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "Name is too long.")]
        public string Item3 { get; set; }

        [StringLength(10, ErrorMessage = "Name is too long.")]
        public string Item4 { get; set; }
    }
}
