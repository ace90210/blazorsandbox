﻿namespace BlazorApp1.Client.Models
{
    public class ChatUser
    {
        public string Username { get; set; }

        public bool IsTyping { get; set; }
    }
}
