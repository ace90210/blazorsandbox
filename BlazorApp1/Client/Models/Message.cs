﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp1.Client.Models
{
    public class Message
    {
        public string Username { get; set; }

        public string Text { get; set; }
    }
}
