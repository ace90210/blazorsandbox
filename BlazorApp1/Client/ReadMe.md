﻿pre-requisites
==============

VS 2019 with Core SDK 3.1
Install web compile extension from here: 
https://marketplace.visualstudio.com/items?itemName=MadsKristensen.WebCompiler

follow this guide to setup auto compiling scss:
https://raaaimund.github.io/tech/2019/11/18/blazor-components-scss-webcompiler/