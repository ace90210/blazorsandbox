﻿using Microsoft.AspNetCore.Blazor.Hosting;

namespace BlazorApp1.Client.HelperExtensions
{
    public static class StartupHelpers
    {
        public static void UseStartup<TStartup>(this WebAssemblyHostBuilder builder) where TStartup : IStartup, new()
        {
            var startup = new TStartup();
            startup.ConfigureServices(builder.Services);
        }
    }
}
