﻿using Microsoft.Extensions.DependencyInjection;

namespace BlazorApp1.Client.HelperExtensions
{
    public interface IStartup
    {
        void ConfigureServices(IServiceCollection services);
    }
}
