﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BlazorApp1.Client.HelperExtensions
{
    public static class HttpExtensions
    {
        public static async Task<HttpResponseMessage> PostJsonTrackedAsync(this HttpClient client, string uri, object content)
        {
            var stringContent = new StringContent(JsonSerializer.Serialize(content), System.Text.Encoding.UTF8, "application/json");

            return await client.PostAsync(uri, stringContent);
        }

        public static async Task<T> GetContentInstanceAsync<T>(this HttpResponseMessage httpResponseMessage)
        {
            try
            {
                var responseContentStream = await httpResponseMessage.Content.ReadAsStreamAsync();

                return await JsonSerializer.DeserializeAsync<T>(responseContentStream, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true, IgnoreNullValues = true });
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static async Task<string> GetContentInstanceAsync(this HttpResponseMessage httpResponseMessage)
        { 
            try
            {
                var responseContentString = await httpResponseMessage.Content.ReadAsStringAsync();
                Console.WriteLine("Response as string\n" + responseContentString);
                return responseContentString;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }    
}
